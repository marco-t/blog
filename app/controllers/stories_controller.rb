class StoriesController < ApplicationController
	before_action :signed_in_user, only: [:new, :create, :update, :destroy]

	def index
		@story = Story.all
	end

	def show
		@story = Story.find(params[:id])
	end

	def new
		@story = Story.new
	end

	def create
		@story = current_user.stories.build(story_params)
		if @story.save
			flash[:success] = "Story Posted!"
			redirect_to @story
		else
			render 'new'
		end

	end

	def edit
		@story = Story.find(params[:id])
	end

	def update
	end

	def destroy
		@story = Story.find(params[:id])
		@story.destroy
		flash[:success] = "Story deleted."
		redirect_to user_path(@story.user)
	end

	private

		def story_params
			params.require(:story).permit(:title, :content)
		end
end
